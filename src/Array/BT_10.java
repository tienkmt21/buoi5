package Array;

public class BT_10 {
    public static void main(String[] args) {
        int[] ints = new int[] {-1,3,-25,5,62,73,25};
        System.out.println("Max value " + findMaxValue(ints));
        System.out.println("Min value " + findMinValue(ints));
    }

    static int findMaxValue(int[] ints){
        int maxValue = ints[0];
        for (int i = 1; i < ints.length; i++) {
            if (maxValue < ints[i]) {
                maxValue = ints[i];
            }
        }
        return maxValue;
    }

    static int findMinValue(int[] ints){
        int minValue = ints[0];
        for (int i = 1; i < ints.length; i++) {
            if (minValue > ints[i]) {
                minValue = ints[i];
            }
        }
        return minValue;
    }
}
