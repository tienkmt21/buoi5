package Array;

public class BT_23 {
    public static void main(String[] args) {
        String[] aStrings = new String[] {"Mot","Hai","Ba"};
        String[] arStrings = new String[] {"Mot","Hai","Ba"};
        String[] strings = new String[] {"Mot","Hai","Bon"};

        System.out.println("Two array equals " + compareArray(arStrings,aStrings));
        System.out.println("Two array equals " + compareArray(arStrings,strings));
    }

    static boolean compareArray(String[] strings1, String[] strings2){
        boolean isEqual = true;
        if (strings1.length == strings2.length){
            for (int i = 0; i < strings1.length; i++) {
                if (!strings1[i].equals(strings2[i])) {
                    isEqual = false;
                }
            }
        } else {
            isEqual = false;
        }
        return isEqual;
    }
}
