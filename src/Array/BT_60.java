package Array;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

public class BT_60 {
    public static void main(String[] args) {
        int[] ints = new int[] {83,-1,3,-25,5,62,73,25,45,25,34};
        shuffle(ints);
    }

     static void shuffle(int[] ints)
    {
        for (int i = ints.length - 1; i >= 1; i--)
        {
            Random random = new Random();
            int j = random.nextInt(i + 1);
            int temp = ints[i];
            ints[i] = ints[j];
            ints[j] = temp;
        }
        System.out.println(Arrays.toString(ints));
    }
}
