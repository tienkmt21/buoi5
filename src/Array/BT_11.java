package Array;

import java.util.Arrays;

public class BT_11 {
    public static void main(String[] args) {
        int[] ints = new int[] {83,-1,3,-25,5,62,73,25,45,25,34};
        int[] newInts = reverse(ints);

        System.out.println(Arrays.toString(newInts));
    }

    static int[] reverse(int[] ints){
        int[] newInts = new int[ints.length];
        int index = 1;
        for (int i = ints.length; i > 0; i--) {
            newInts[index-1] = ints[i-1];
            index++;
        }
        return newInts;
    }
}
