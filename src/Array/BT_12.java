package Array;

public class BT_12 {
    public static void main(String[] args) {
        int[] ints = new int[] {83,-1,3,-25,5,62,73,-1,25,45,25,34,-1,-1,-1,-1,-1};
        findDuplicate(ints);
    }

    static void findDuplicate(int[] ints){
        int[] intDuplicate = new int[ints.length/2];
        int index =0;
        for (int i = 0; i < ints.length; i++) {
            for (int j = i+1; j < ints.length; j++) {
                if (ints[i] == ints[j] & !isExist(intDuplicate,ints[i])){
                    System.out.println("Duplicate Element is : "+ ints[j]);
                    intDuplicate[index] = ints[i];
                    index++;
                }
            }
        }
    }

    static boolean isExist(int[] ints, int value){
        for (int i = 0; i < ints.length; i++) {
            if ( ints[i] == value ){
                return true;
            }
        }
        return false;
    }
}
