package Array.Bai_1;

public class B_2b {
    static void sortId(Student[] student) {
        Student studentTemp;
        for (int i = 0; i < student.length; i++) {
            for (int j = 0; j < student.length - i - 1; j++) {
                if (student[j].getId() < student[j+1].getId()) {
                    studentTemp = student[j];
                    student[j] = student[j+1];
                    student[j+1] = studentTemp;
                }
            }
        }
    }
}
