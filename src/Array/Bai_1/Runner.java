package Array.Bai_1;

import java.time.LocalDate;

public class Runner {
    public static void main(String[] args) {
        Student[] students = student();
        Student sv7 = new Student(7,"Nguyen Van G","CNTT", LocalDate.parse("2000-07-03"));
        Student sv8 = new Student(6,"Le Van P","Hacker White Hat", LocalDate.parse("1998-09-23"));

//        sortName(students);
//        for (int i = 0; i < students.length; i++) {
//            System.out.println(students[i].getName());
//        }

//        Student[] newStudent1 = B_1a.addStudent(students,sv7);
//        Student[] newStudent2 = B_1a.addStudent(students,sv8);
//        for (int i = 0; i < newStudent1.length; i++) {
//            System.out.println(newStudent1[i].getName());
//        }
//
//        for (int i = 0; i < newStudent2.length; i++) {
//            System.out.println(newStudent2[i].getName());
//        }

//        B_2b.sortId(students);
//        for (int i = 0; i < students.length; i++) {
//            System.out.println(students[i]);
//        }

//        B_4a.findMajor(students);
    }

    public static Student[] student(){
        Student sv1 = new Student(1,"Nguyen Van A","CNTT", LocalDate.parse("2002-05-19"));
        Student sv2 = new Student(2,"Tran Thi V","CEO", LocalDate.parse("1980-07-14"));
        Student sv3 = new Student(3,"Pham Minh C","CNTT", LocalDate.parse("2000-03-14"));
        Student sv4 = new Student(4,"Hoang Van F","Worker", LocalDate.parse("1999-04-12"));
        Student sv5 = new Student(5,"Ly Thi S","Farmer", LocalDate.parse("1991-06-24"));
        Student sv6 = new Student(6,"Kieu Quoc D","CNTT", LocalDate.parse("2004-05-04"));
        return new Student[]{sv1,sv2,sv3,sv4,sv5,sv6};
    }

    static void sortName(Student[] student) {
        Student studentTemp;
        for (int i = 0; i < student.length; i++) {
            for (int j = 0; j < student.length - i - 1; j++) {
                if (student[j].getName().compareTo(student[j + 1].getName()) > 0) {
                    studentTemp = student[j];
                    student[j] = student[j+1];
                    student[j+1] = studentTemp;
                }
            }
        }
    }
}
