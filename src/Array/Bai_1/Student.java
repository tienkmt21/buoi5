package Array.Bai_1;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Student {
    private int id;
    private String name;
    private String major;
    private String birthDay;
    private LocalDate localDate;
    public Student (int id, String name, String major, LocalDate birthDay) {
        this.id = id;
        this.major = major;
        this.name = name;
        this.birthDay = birthDay.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy"));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String date) {
        this.birthDay = date;
    }

    @Override
    public String toString() {
        String text = "Id=%d, Name=%s, Major=%s, BirthDay=%s ";
        return String.format(text, this.id, this.name, this.major, this.birthDay);
    }
}
