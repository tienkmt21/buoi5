package Array;

public class BT_27 {
    public static void main(String[] args) {
        int[] ints = new int[] {1,2,3,5,7,8,4,2,15,5,7,5,2,0};
        countEvenOdd(ints);
    }

    static void countEvenOdd(int[] ints){
        int countEven = 0;
        int countOdd = 0;
        for (int i = 0; i < ints.length; i++) {
            if (ints[i]%2 ==0){
                countEven++;
            } else {
                countOdd++;
            }
        }
        System.out.println("Count event " +countEven + "\n" +
                "Count odd " + countOdd);
    }
}
