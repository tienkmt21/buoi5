package Array;

public class BT_6 {
    public static void main(String[] args) {
        String[] strings = new String[]{"Mot","Hai","Ba","Bon","Nam","Ba"};
        findIndex(strings,"Ba");
    }

    static void findIndex(String[] strings,String find){
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].equals(find)){
                System.out.println(String.format("Index of %s in array is %d",find,i));
            } else if (i == (strings.length-1)) {
                System.out.println("String does not exist!");
            }
        }
    }
}
