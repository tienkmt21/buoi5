package Array;

public class BT_33 {
    public static void main(String[] args) {
        String[] strings = new String[]{"Mot","Hai","Hai","Bon","Nam","","Ba","Ba","Ba","Mot","Chin","Hai",""};
        System.out.println("New length of the array " + newLength(strings));
    }

    static int newLength(String[] strings){
        String[] temp = new String[strings.length];
        int index = 0;
        for (int i = 0; i < strings.length; i++) {
            if (!isExist(temp,strings[i])){
                temp[index] = strings[i];
                index++;
            }
        }
        return index-1;
    }

    static boolean isExist(String[] strings, String text){
        for (int i = 0; i < strings.length; i++) {
            if (text.equals(strings[i])){
                return true;
            }
        }
        return false;
    }
}
