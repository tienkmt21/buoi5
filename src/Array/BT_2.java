package Array;

public class BT_2 {
    public static void main(String[] args) {
        int[] ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int sum = 0;

        for (int i : ints) {
            sum += i;
        }
        System.out.println("The sum is " + sum);
    }
}
