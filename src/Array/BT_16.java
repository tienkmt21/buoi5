package Array;

import java.util.Arrays;

public class BT_16 {
    public static void main(String[] args) {
        String[] strings = new String[]{"Mot","Hai","Hai","Bon","Nam","","Ba","Ba","Ba","Mot","Chin","Hai",""};
        String[] stringsAfterRemove = removeDuplicate(strings);

        System.out.println(Arrays.toString(stringsAfterRemove));
    }

    static String[] removeDuplicate(String[] strings){
        String[] temp = new String[strings.length];
        int index = 0;
        for (int i = 0; i < strings.length; i++) {
            if (!isExist(temp,strings[i])){
                temp[index] = strings[i];
                index++;
            }
        }
        String[] stringsAfterRemove = new String[index-1];
        for (int i = 0; i < index-1; i++) {
            stringsAfterRemove[i] = temp[i];
        }
        return stringsAfterRemove;
    }

    static boolean isExist(String[] strings, String text){
        for (int i = 0; i < strings.length; i++) {
            if (text.equals(strings[i])){
                return true;
            }
        }
        return false;
    }
}
