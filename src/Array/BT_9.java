package Array;

import java.util.Arrays;

public class BT_9 {
    public static void main(String[] args) {
        String[] strings = new String[]{"Mot","Hai","Ba","Bon","Nam","Ba"};
        String[] newStrings = insert(strings,"Chin",7);
        System.out.println(Arrays.toString(newStrings));
    }

    static String[] insert(String[] strings,String text ,int index){
        if (index > strings.length){
            System.out.println("Index out of bounds for length");
            return strings;
        }
        String[] newStrings = new String[strings.length+1];
        boolean isInsert = false;
        for (int i = 0; i <= strings.length; i++) {
            if (i == index){
                newStrings[i] = text;
                isInsert = true;
            } else {
                if (isInsert) {
                    newStrings[i] = strings[i-1];
                } else {
                    newStrings[i] = strings[i];
                }
            }
        }
        return newStrings;
    }
}
