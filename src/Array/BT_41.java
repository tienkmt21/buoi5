package Array;

public class BT_41 {
    public static void main(String[] args) {
        int[] ints = new int[] {1,12,43,5444,757,3,2,95,-87};
        findSmallest(ints);
        findSecondSmallest(ints);
    }

    static void findSmallest(int[] ints){
        int smallest = ints[0];
        for (int i = 1; i < ints.length; i++) {
            if (smallest > ints[i]) {
                smallest = ints[i];
            }
        }
        System.out.println("Smallest is " + smallest);
    }

    static void findSecondSmallest(int[] ints){
        if (ints.length < 2){
            System.out.println("Second smallest no exist");
            return;
        }
        int smallest = ints[0];
        int secondSmallest = ints[1];
        for (int i = 1; i < ints.length; i++) {
            if (smallest > ints[i]) {
                secondSmallest = smallest;
                smallest = ints[i];
            } else if (ints[i] < secondSmallest){
                secondSmallest = ints[i];
            }
        }
        System.out.println("Second smallest is " + secondSmallest);
    }
}
