package Array;

public class BT_13 {
    public static void main(String[] args) {
        String[] strings = new String[]{"Mot","Hai","Ba","Bon","Nam","Ba","Ba","Ba","Mot","Chin","Hai"};
        findStringDuplicate(strings);
    }

    static void findStringDuplicate(String[] strings){
        String[] stringsDuplicate = new String[strings.length/2];
        int index =0;
        for (int i = 0; i < strings.length; i++) {
            for (int j = i+1; j < strings.length; j++) {
                if (strings[i].equals(strings[j]) & !isExist(stringsDuplicate,strings[i])){
                    System.out.println("Duplicate Element is : "+ strings[j]);
                    stringsDuplicate[index] = strings[i];
                    index++;
                }
            }
        }
    }

    static boolean isExist(String[] strings, String text){
        for (int i = 0; i < strings.length; i++) {
            if (text.equals(strings[i])){
                return true;
            }
        }
        return false;
    }
}
