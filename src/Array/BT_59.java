package Array;

public class BT_59 {
    public static void main(String[] args) {
        int[] ints = new int[] {1,12,43,5444,757,3,2,95,-87};
        maxProduct(ints);
    }

    static void maxProduct(int[] ints){
        int maxProduct =0;
        int[] ints1 = new int[2];
        for (int i = 0; i < ints.length; i++) {
            for (int j = 1; j < ints.length; j++) {
                if (ints[i]*ints[j] > maxProduct && ints[i] != ints[j]){
                    maxProduct = ints[i]*ints[j];
                    ints1[0] = ints[i];
                    ints1[1] = ints[j];
                }
            }
        }
        System.out.println(String.format("Pair is (%d, %d), Maximum Product: %s",ints1[0],ints1[1],maxProduct));
    }
}
