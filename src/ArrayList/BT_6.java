package ArrayList;

import java.util.ArrayList;
import java.util.Collections;

public class BT_6 {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(4);
        arrayList.add(7);
        arrayList.add(3);
        arrayList.add(6);

        System.out.println("ArrayList after reverse " + reverseScores(arrayList));
    }

    public static ArrayList<Integer> reverseScores(ArrayList<Integer> scoreBoard) {
        Collections.reverse(scoreBoard);
        return scoreBoard;
    }
}
