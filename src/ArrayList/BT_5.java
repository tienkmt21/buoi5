package ArrayList;

import java.util.ArrayList;

public class BT_5 {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(4);
        arrayList.add(7);
        arrayList.add(3);
        arrayList.add(6);
        System.out.println("Average is " + average(arrayList));
    }

    public static double average(ArrayList<Integer> scoreBoard) {
        double average =0;
        int sumScores = 0;
        for (Integer ints: scoreBoard) {
            sumScores += ints;
        }
        return average = (double) sumScores / scoreBoard.size();
    }
}
