package ArrayList;

import java.util.ArrayList;

public class BT_10 {
    public static void main(String[] args) {
        ArrayList<String> arrayList1 = new ArrayList<>();
        arrayList1.add("Mot");
        arrayList1.add("Ba");
        arrayList1.add("Bon");
        ArrayList<String> arrayList2 = new ArrayList<>();
        arrayList2.add("Hai");
        arrayList2.add("Bon");
        arrayList1.add("Ba");

        System.out.println(commonValues(arrayList1,arrayList2));
    }

    public static ArrayList<String> commonValues( ArrayList<String> values1, ArrayList<String> values2){
        ArrayList<String> newArrayList = new ArrayList<>();
        newArrayList.addAll(values1);
        newArrayList.addAll(values2);
        for (int i = 0; i < newArrayList.size(); i++) {
            if (values1.contains(newArrayList.get(i)) || values2.contains(newArrayList.get(i))) {
                newArrayList.remove(i);
            }
        }
        return newArrayList;
    }
}
