package ArrayList;

import java.util.ArrayList;

public class BT_9 {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Mot");
        arrayList.add("Ba");
        arrayList.add("Bon");
        arrayList.add("Hai");
        arrayList.add("Mot");
        arrayList.add("Bon");

        System.out.println(removeDuplicates(arrayList));
    }

    public static ArrayList<String> removeDuplicates(ArrayList<String> values) {
        ArrayList<String> newArrayList = new ArrayList<>();
        for (String strings : values) {
            if (!newArrayList.contains(strings)) {
                newArrayList.add(strings);
            }
        }
        return newArrayList;
    }
}
