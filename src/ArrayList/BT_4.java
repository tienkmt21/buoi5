package ArrayList;

import java.util.ArrayList;

public class BT_4 {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(4);
        arrayList.add(7);
        arrayList.add(3);
        arrayList.add(6);
        System.out.println("High Scores is " + getHighScore(arrayList));
    }

    public static int getHighScore(ArrayList<Integer> scoreBoard) {
        int highScore = scoreBoard.get(0);
        for (int i = 0; i < scoreBoard.size(); i++) {
            if (scoreBoard.get(i) > highScore) {
                highScore = scoreBoard.get(i);
            }
        }
        return highScore;
    }
}
