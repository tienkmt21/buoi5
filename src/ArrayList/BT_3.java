package ArrayList;

import java.util.ArrayList;

public class BT_3 {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(4);
        arrayList.add(7);
        arrayList.add(3);
        arrayList.add(6);
        System.out.println("Sum Scores is " + sumScores(arrayList));
    }

    public static int sumScores(ArrayList<Integer> scoreBoard) {
        int sumScores = 0;
        for (Integer ints: scoreBoard) {
            sumScores += ints;
        }
        return sumScores;
    }
}
