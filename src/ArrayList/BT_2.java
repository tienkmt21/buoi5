package ArrayList;

import Array.Bai_1.Runner;
import Array.Bai_1.Student;

import java.util.ArrayList;
import java.util.Arrays;

public class BT_2 {
    public static void main(String[] args) {
        Student[] students = Runner.student();
        ArrayList<Student> arrayList = new ArrayList<>();
        arrayList.addAll(Arrays.asList(students));

        System.out.println(arrayList.get(0).getName());
    }
}
